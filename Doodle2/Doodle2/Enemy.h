//
//  Enemy.h
//  Doodle2
//
//  Created by Mariuxi Ana Yagual on 4/7/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Enemy : UIImageView
@property (nonatomic) float dx, dy;
@end
