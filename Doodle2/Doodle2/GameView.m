//
//  GameView.m
//  Doodle2
//
//  Created by Patrick Madden on 2/4/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "GameView.h"

@implementation GameView
@synthesize jumper, bricks, zubats;
@synthesize scoreview;
@synthesize tilt;

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self startGame];
    }
    return self;
}

-(void)startGame{
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back.jpg"]];
    CGRect bounds = [self bounds];
    scoreview = [[Score alloc] initWithFrame:CGRectMake(bounds.size.width-50, 2, 80, 40)];
    [scoreview construct];
    jumper = [[Jumper alloc] initWithFrame:CGRectMake(bounds.size.width/2, bounds.size.height - 20, 100, 100)];
    //set image to jumper
    UIGraphicsBeginImageContext(self.jumper.frame.size);
    [[UIImage imageNamed:@"piki.png"] drawInRect:self.jumper.bounds];
    UIImage *imag = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [jumper setBackgroundColor : [UIColor colorWithPatternImage:imag]];
    
    [jumper setDx:0];
    [jumper setDy:7];//5
    [self addSubview:scoreview];
    [self addSubview:jumper];
    [self makeBricks:nil];
    [self makeZubats:nil];
}


-(IBAction)makeBricks:(id)sender
{
    CGRect bounds = [self bounds];
    float width = bounds.size.width * .2;//.2
    float height = 50;
    
    if (bricks)
    {
        for (Brick *brick in bricks)
        {
            [brick removeFromSuperview];
        }
    }
    
    bricks = [[NSMutableArray alloc] init];
    for (int i = 0; i < 15; ++i)//10
        {
            Brick *b = [[Brick alloc] initWithFrame:CGRectMake(0, 0, width, height)];
            //set image
            UIGraphicsBeginImageContext(b.frame.size);
            [[UIImage imageNamed:@"orange-cloud.png"] drawInRect:b.bounds];
            UIImage *imag = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [b setBackgroundColor : [UIColor colorWithPatternImage:imag]];

            [self addSubview:b];
            [b setCenter:CGPointMake(rand() % (int)(bounds.size.width * 1), rand() % (int)(bounds.size.height * 1))];//1 for both
            [b setDy: 0];
            [bricks addObject:b];
            
        }
}

-(IBAction)makeZubats:(id)sender
{
    CGRect bounds = [self bounds];
    float width = 80;//.2
    float height = 80;
    
    if (zubats)
    {
        for (Enemy *zub in zubats)
        {
            [zub removeFromSuperview];
        }
    }
    
    zubats = [[NSMutableArray alloc]init];
    for (int i = 0; i < 3; ++i)//10
    {
        Enemy *b = [[Enemy alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        //set image
        UIGraphicsBeginImageContext(b.frame.size);
        //[[UIImage animatedImageNamed:@"zubat.gif" duration:1.0f] drawInRect:b.bounds];
        [[UIImage imageNamed:@"zubat.gif"] drawInRect:b.bounds];
        UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        b.image = img;
        
        [self addSubview:b];
        [b setCenter:CGPointMake(rand() % (int)(bounds.size.width * 1), rand() % (int)(bounds.size.height * 1))];//1 for both
        [b setDy: 0];
        [zubats addObject:b];
        
    }

}

-(void)arrange:(CADisplayLink *)sender
{
 //   CFTimeInterval ts = [sender timestamp];
    
    CGRect bounds = [self bounds];
    
    // Apply gravity to the acceleration of the jumper
    [jumper setDy:[jumper dy] - .3];
    
    //foreach brick give positive accelation until reach 0.
    //if accelaration still negative, positive then keep decreasing else 0
    for (Brick *brick in bricks)
    {
        if([brick dy] < 0){
            [brick setDy:[brick dy] + .2];
        }
        else{
            [brick setDy:0];
        }
    }
    
    for (Enemy *en in zubats){
        if([en dy] < 0){
            [en setDy:[en dy] + .2];
        }
        else{
            [en setDy:0];
        }
    }
        
    // Apply the tilt.  Limit maximum tilt to + or - 5
    [jumper setDx:[jumper dx] + tilt];
    if ([jumper dx] > 5)
        [jumper setDx:5];
    if ([jumper dx] < -5)
        [jumper setDx:-5];

    
    // Jumper moves in the direction of gravity
    CGPoint p = [jumper center];
    p.x += [jumper dx];
    p.y -= [jumper dy];
    
    for (Brick *brick in bricks)
    {
        CGPoint bp = [brick center];
        bp.x += [brick dx];
        bp.y -= [brick dy];
        
        if(bp.y > bounds.size.height){
            bp.y = 0;
            bp.x = rand() % (int)(bounds.size.width * 1);
        }
        //for jumper to not go beyond the top of screen
         //to wrap around
        [brick setCenter:bp];
    }
    
    for (Enemy *e in zubats)
    {
        CGPoint t = [e center];
        t.x += [e dx];
        t.y -= [e dy];
        
        if(t.y > bounds.size.height){
            t.y = 0;
            //randomizing x
            t.x = rand() % (int)(bounds.size.width * 1);
        }
        //to wrap around
        [e setCenter:t];
    }
    
    //if we touch a zubat
    for(Enemy *e in zubats){
        CGRect eb = [e frame];
        if (CGRectContainsPoint(eb, p))
        {
            NSLog(@"touched zubat");
            //make it spin
           // [NSTimer scheduledTimerWithTimeInterval:1.0 target:jumper selector:@selector(runSpinAnimationOnView) userInfo:nil repeats:YES];
            [jumper removeFromSuperview];
            [self bannerGameOver];
        }
    }
    
    // If the jumper has fallen below the bottom of the screen,//should die then
    if (p.y > bounds.size.height)
    {
      //  [jumper setDy:9];
      //  p.y = bounds.size.height;
        [jumper removeFromSuperview];
       // [self performSelector:@selector(playAgain) withObject:nil afterDelay:1.5];
        [self bannerGameOver];
    }
    
    // If we've gone past the top of the screen, wrap around
    if (p.y < 0)
        p.y += bounds.size.height;
    
    // If we have gone too far left, or too far right, wrap around
    if (p.x < 0){
        p.x += bounds.size.width;
    }
    if (p.x > bounds.size.width){
        p.x -= bounds.size.width;
    }
    
    // If we are moving down, and we touch a brick, we get
    // a jump to push us up.
    if ([jumper dy] < 0 && [jumper superview])
    {
        for (Brick *brick in bricks)
        {
            CGRect b = [brick frame];
            if (CGRectContainsPoint(b, p))
            {
              //  NSLog(@"now, and %f and dy is %f", p.y, jumper.dy);
                // Yay!  Bounce!
             //   NSLog(@"Bounce!");
                [scoreview increaseCount:25];
                if(p.y < bounds.size.height/2){
                    [jumper setDy:4];
                    for (Brick *br in bricks)
                    {
                        [br setDy:-11];
                    }
                    for(Enemy *e in zubats){
                        [e setDy:-11];
                    }
                }
                else{
                    [jumper setDy:9];
                    for (Brick *br in bricks)
                    {
                        [br setDy:-9];
                    }
                    for(Enemy *e in zubats){
                        [e setDy:-9];
                    }
                }
            }
        }
    }
    [jumper setCenter:p];
}

-(void)bannerGameOver{
    CGRect bounds = [self bounds];
    UILabel *banner = [[UILabel alloc] initWithFrame:CGRectMake(40, 30, bounds.size.width - 10, bounds.size.height/2)];
    [banner setText:@"GAME OVER"];
    [banner setTextColor:[UIColor blackColor]];
    [banner setFont:[UIFont fontWithName:@"Trebuchet MS" size:64]];
    [self addSubview:banner];
}

-(void)runSpinAnimationOnView{
    [jumper setTransform:CGAffineTransformMakeRotation(10*(M_PI/360))];

}

@end
