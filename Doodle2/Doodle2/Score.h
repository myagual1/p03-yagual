//
//  Score.h
//  Doodle2
//
//  Created by Mariuxi Ana Yagual on 4/8/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Score : UIView

@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UILabel *counter;
@property (nonatomic) int countnum;

-(void)construct;
-(void)increaseCount:(int)num;

@end
