//
//  Score.m
//  Doodle2
//
//  Created by Mariuxi Ana Yagual on 4/8/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import "Score.h"

@implementation Score

@synthesize label, counter, countnum;

-(void)construct{
    CGFloat borderWidth = 2.0f;
    self.frame = CGRectInset(self.frame, -borderWidth, -borderWidth);
    self.layer.borderColor = [UIColor orangeColor].CGColor;
    self.layer.borderWidth = borderWidth;
    [self setBackgroundColor:[UIColor cyanColor]];
    CGRect bounds = [self bounds];
    
    //counter setup
    counter = [[UILabel alloc]initWithFrame:CGRectMake(bounds.origin.x, bounds.size.height/2 -2, bounds.size.width, bounds.size.height/2)];
    [counter setText:@"0"];
    counter.textAlignment = NSTextAlignmentCenter;
    [counter setFont:[UIFont fontWithName:@"Trebuchet MS" size:16]];

    //label setup
    label = [[UILabel alloc]initWithFrame:CGRectMake(bounds.origin.x, bounds.origin.y, bounds.size.width, bounds.size.height/2)];
    [label setText:@"Score"];
    [label setTextColor:[UIColor blackColor]];
    [label setFont:[UIFont fontWithName:@"Trebuchet MS-Bold" size:14]];
    label.textAlignment = NSTextAlignmentCenter;

    [self addSubview:counter];
    [self addSubview:label];
}

-(void)increaseCount:(int)num{
    countnum += num;
    NSString *rString = [NSString stringWithFormat:@"%d", countnum];
    [counter setText:rString];
}


@end
