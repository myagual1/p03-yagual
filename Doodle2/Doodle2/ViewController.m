//
//  ViewController.m
//  Doodle2
//
//  Created by Patrick Madden on 2/4/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic, strong) CADisplayLink *displayLink;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"glitter.jpg"]];
    // Do any additional setup after loading the view, typically from a nib.
    _displayLink = [CADisplayLink displayLinkWithTarget:_gameView selector:@selector(arrange:)];
    [_displayLink setPreferredFramesPerSecond:30];
    [_displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)speedChange:(id)sender
{
    UISlider *s = (UISlider *)sender;
    // NSLog(@"tilt %f", (float)[s value]);
    [_gameView setTilt:(float)[s value]];
}

-(void)playAgain{
    CGRect bounds = [_gameView bounds];
    UIView *gamov = [[UIView alloc]initWithFrame:bounds];
    [gamov setBackgroundColor:[UIColor whiteColor]];
    UILabel *banner = [[UILabel alloc] initWithFrame:CGRectMake(40, 30, bounds.size.width - 10, bounds.size.height/2)];
    [banner setText:@"GAME OVER"];
    [banner setTextColor:[UIColor blackColor]];
    [banner setFont:[UIFont fontWithName:@"Trebuchet MS" size:64]];
    //start over button
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self action:@selector(clickedTry:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Try Again" forState:UIControlStateNormal];
    button.frame = CGRectMake(bounds.size.width/4, bounds.size.height/2, bounds.size.height/3, 50);
    button.backgroundColor = [UIColor orangeColor];
    button.tintColor = [UIColor whiteColor];
    [button.titleLabel setFont : [UIFont fontWithName:@"Trebuchet MS" size:20]];
    
    [gamov addSubview:button];
    [gamov addSubview:banner];
    [self.view addSubview:gamov];
    
  //  NSLog(@"end of play again");
}

-(IBAction)clickedTry:(id)sender{
    [_gamov removeFromSuperview];
}

@end
