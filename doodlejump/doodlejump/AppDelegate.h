//
//  AppDelegate.h
//  doodlejump
//
//  Created by Mariuxi Ana Yagual on 2/25/17.
//  Copyright © 2017 Binghamton University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

