//
//  ViewController.h
//  doodlejump
//
//  Created by Mariuxi Ana Yagual on 2/25/17.
//  Copyright © 2017 Binghamton University. All rights reserved.
//


#import <QuartzCore/CAAnimation.h>
#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>

#define kUpdateInterval (1.0f / 60.0f)

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *pacman;
@property (strong, nonatomic) IBOutlet UIImageView *villain;
@property (strong, nonatomic) IBOutlet UIImageView *star;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *cloud;

@property (assign, nonatomic) CGPoint currentPoint;
@property (assign, nonatomic) CGPoint previousPoint;
@property (assign, nonatomic) CGFloat pacmanXVelocity;
@property (assign, nonatomic) CGFloat pacmanYVelocity;
@property (assign, nonatomic) CGFloat angle;
@property (assign, nonatomic) CMAcceleration acceleration;
@property (retain, nonatomic) CMMotionManager  *motionManager;
@property (strong, nonatomic) NSOperationQueue *queue;
@property (strong, nonatomic) NSDate *lastUpdateTime;

@end

